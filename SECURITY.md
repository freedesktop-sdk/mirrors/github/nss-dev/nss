# Security Policy

## Supported Versions

The NSS team will provide support for the current version of NSS as well as the
the ESR version and its point releases.

## Reporting a Vulnerability

Security vulnerabilities should be reported to Bugzilla.
https://bugzilla.mozilla.org/enter_bug.cgi?product=NSS

Please do not forget to mark the bug as sensitive before submitting it.
